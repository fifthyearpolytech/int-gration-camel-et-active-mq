package com.aos;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.jms.JmsConsumer;
import org.apache.camel.component.jms.JmsEndpoint;
import org.apache.camel.impl.DefaultCamelContext;

public class Camel {

	private static ConnectionFactory connectionFactory;

	public static void main(String[] args) {
		
		try {
			Context ci = new InitialContext();
			connectionFactory = (ConnectionFactory) ci.lookup("connectionFactory");
			CamelContext context = new DefaultCamelContext();
			
			FournisseurService fournisseur = FournisseurService.getInstance();
			
			context.addComponent("jms-test",
					JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
			
			ProducerTemplate pt = context.createProducerTemplate();
			pt.setDefaultEndpointUri("jms-test:MyQueue");
			
			context.addRoutes(new RouteBuilder() {
				public void configure() throws Exception {
					from("jms-test:MyQueue").process(new Processor() {
						
						@Override
						public void process(Exchange exchange) throws Exception {
							
							Message message = (Message) exchange.getIn();
							String articleId = message.getBody(String.class);
							System.out.println("Message reçu : " + articleId);
							System.out.println(message.getMessageId());
							
							
							Float price = fournisseur.getPrix(articleId);
							//System.out.println("prix : " + price);
							
							//message.setBody(price, Float.class);
							//exchange.setMessage(message);
							
//							JmsEndpoint responseEndPoint = (JmsEndpoint)
//									context.getEndpoint("jms-test:MyQueue");
							
							
							pt.sendBody(price.toString());
							
						}
					});//.to("jms-test:MyQueue");
						
				}
			});
			
			context.start();
			// ajouter ici un code permettant de pauser le thread courant
			System.out.println("attente de message : ");
			//new Thread().sleep(1000);
			
			while(true) {
				
			}
			//context.stop();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

}
