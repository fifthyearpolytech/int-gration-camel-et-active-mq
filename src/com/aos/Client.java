package com.aos;


import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.stream.events.EndDocument;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.jms.JmsConsumer;
import org.apache.camel.component.jms.JmsEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.destination.DestinationResolver;

public class Client implements MessageListener, MessageListenerContainer {

	private ConnectionFactory connectionFactory;
	private Destination destination;
	Session session;
	private MessageProducer producer;
	private String message;
	private Connection connection;
	
	CamelContext context = new DefaultCamelContext();

	public Client(String m) {
		this.message = m;
		try {
			this.connect();
			this.sendMessages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		
//		JmsEndpoint responseEndPoint = (JmsEndpoint)
//				context.getEndpoint("jms-test:MyQueue");
//		
//		try {
//			JmsConsumer consumer = responseEndPoint.createConsumer(new Processor() {
//				public void process(Exchange e) throws Exception {
//				System.out.println("Réponse reçue : " + e.getIn().getBody());
//				}});
//		} catch (Exception e1) {
//		}
		

		String messText = null;
		try {
			if (message instanceof TextMessage) {
				TextMessage msg = (TextMessage) message;
				messText = msg.getText();
				System.out.println("Message reçu = " + msg.getText());
				System.out.println(message.getJMSCorrelationID());
			}
			
//			if (connection != null)
//				connection.close();
//			if (this.session != null)
//				this.session.close();

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	private void connect() throws Exception {
		Context jndiContext = new InitialContext();
		// Lookup de la fabrique de connexion et de la destination
		connectionFactory = (ConnectionFactory) jndiContext.lookup("connectionFactory");
		destination = (Destination) jndiContext.lookup("MyQueue");
	}

	private void sendMessages() throws JMSException {
		// Créer une connexion au système de messagerie
		this.connection = connectionFactory.createConnection();
		connection.start();

		// Création d'une Connexion et d'une Session:
		this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination dest = this.session.createQueue("MyQueue");

		this.producer = this.session.createProducer(dest);
		this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

		Destination tempDest = this.session.createTemporaryQueue();
		MessageConsumer response = this.session.createConsumer(tempDest);
		response.setMessageListener(this);

		TextMessage textMess = this.session.createTextMessage();
		textMess.setText(this.message);

		textMess.setJMSReplyTo(tempDest);
		
		
		String id = RandomStringUtils.random(10);
		textMess.setJMSCorrelationID(id);
		this.producer.send(textMess);
		
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String str = null;
		System.out.println("Votre message : ");
		str = sc.nextLine();
		while (str.compareToIgnoreCase("quit") != 0 ) {
			Client client = new Client(str);
			
			System.out.println("Votre message : ");
			str = sc.nextLine();
		}
		System.out.println("Au revoir");		
		sc.close();
	}

	@Override
	public boolean isAutoStartup() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void stop(Runnable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isRunning() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPhase() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DestinationResolver getDestinationResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageConverter getMessageConverter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isPubSubDomain() {
		// TODO Autoz-generated method stub
		return false;
	}

	@Override
	public boolean isReplyPubSubDomain() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setupMessageListener(Object arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0.toString());
		
	}

}
