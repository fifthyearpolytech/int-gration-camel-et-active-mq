package com.aos;

import java.util.HashMap;
import java.util.Map;

public class FournisseurService {
	
	private Map<String, Float> articles ;
	private static FournisseurService instance = null;
	
	private FournisseurService() {
		articles = new HashMap<>();
		articles.put("ordi", 400f);
		articles.put("telephone", 200f);
	}
	
	public static FournisseurService getInstance() {
		if(instance == null)
			return new FournisseurService();
		return instance;
	}
	
	public float getPrix(String idProduit) {
		Float prix = articles.get(idProduit);
		return prix!=null?prix:0;
	}

}
